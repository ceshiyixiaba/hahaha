---
home: true
heroImage: /logo.jpg
actionText: 开始阅读 →
actionLink: /1-Introduction/0
footer: MIT Licensed | Copyright © 2018 Catch
footer: MIT Licensed | Copyright © 2019 Catch
---

---
home: true
heroImage: /logo.jpg
actionText: 开始阅读 →
actionLink: /1-Introduction/0
footer: MIT Licensed | Copyright © 2018 Catch
footer: MIT Licensed | Copyright © 2019 Catch
---

```math
 = \begin{cases} 1/\rho & \quad \rho \ne 0, \
+\infty & \quad \rho = 0, \
0 & \quad \rho = +\infty. \
\end{cases} $$

$x = \begin{cases}a &\text{if } b \ c &\text{if } d \end{cases}$

$$ x = \begin{cases} a &\text{if } b \ c &\text{if } d \end{cases} $$

$$ \begin{vmatrix} a & b \ c & d \end{vmatrix} $$

$$ R = \begin{cases} 1/\rho & \quad \rho \ne 0, \ +\infty & \quad \rho = 0, \ 0 & \quad \rho = +\infty. \ \end{cases} $$ $x = \begin{cases}a &\text{if } b \ c &\text{if } d \end{cases}$ $$ x = \begin{cases} a &\text{if } b \ c &\text{if } d \end{cases} $$ $$ \begin{vmatrix} a & b \ c & d \end{vmatrix} $$
```